import Vue from 'vue'
import axios from 'axios'
import { Notify } from 'quasar'

function e(mess){
    Notify.create({
        type: 'negative',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

function s(mess) {
    Notify.create({
        type: 'positive',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

function w(mess) {
    Notify.create({
        type: 'warning',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}

function i (mess){
    Notify.create({
        type: 'info',
        message: mess,
        position: "bottom-right",
        progress: true,
        actions: [
            { icon: 'close', size: "sm", color: 'white', handler: () => { /* ... */ } }
        ]
    })
}
Vue.prototype.$axios = axios
Vue.prototype.$s = s()
Vue.prototype.$i = i()
Vue.prototype.$e = e()
Vue.prototype.$w = w()
Vue.prototype.$domen = "http://127.0.0.1:10/api/"
