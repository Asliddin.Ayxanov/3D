
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      { path: ' ', component: () => import('pages/Index.vue') },
      { path: '/category', component: () => import('pages/category/category.vue') },
      { path: '/object', component: () => import('pages/object/object.vue') },
      { path: '/test', component: () => import('pages/object/object.vue') },
      { path: '/content', component: () => import('pages/contents/contents.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
