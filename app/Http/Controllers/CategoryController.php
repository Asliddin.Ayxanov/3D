<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
class CategoryController extends Controller
{
    public function GetCategory(Request $request)
    {
        $categories = Categories::orderBy('id')->get();
        return $categories;
    }
    public function CreateCategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $data = [
            'name' => $request->name,
        ];

        $category = Categories::create([
            'name' => $request->name,
        ]);
        return $category;
    }
    public function DeleteCategory(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        return Categories::where('id', $request->id)->delete();
    }

    public function UpdateCategory(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|min:2',
        ]);

        $data = [
            'name' => $request->name
        ];
        //bazaga shunaqa nom bilan save qilingan yani stores_id bilan
        $category = Categories::where('id', $request->id)->update($data);
        return $category;
    }
    public function multidelete(Request $request){
        $data = json_decode($request->data);
        $rus=[];
        foreach ($data as $val) {
            // return $val->id;
            
            
            $rus[]= Categories::find($val->id)->delete();
        }
        return $rus;
    }
}
