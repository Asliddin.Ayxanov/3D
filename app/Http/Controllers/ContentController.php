<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contents;
class ContentController extends Controller
{
    public function getcontent()
    {
        return Contents::with('object.file')->with('category')->get();
    }
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'text' => 'required|min:10',
            'category_id' => 'required',
            'object_id' => 'required',
        ]);
        $content = Contents::create([
            'name' => $request->name,
            'text' => $request->text,
            'category_id' => $request->category_id,
            'object_id' => $request->object_id,
        ]);
        return $content;
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'text' => 'required|min:10',
            'category_id' => 'required',
            'object_id' => 'required',
        ]);
        $content = Contents::find($request->id)->update([
            'name' => $request->name,
            'text' => $request->text,
            'category_id' => $request->category_id,
            'object_id' => $request->object_id,
        ]);
        return $content;
    }
    public function delete_content(Request $request)
    {
        return Contents::find($request->id)->delete();
    }
    public function multidelete(Request $request){
        $data = json_decode($request->data);
        $rus=[];
        foreach ($data as $val) {
            // return $val->id;
            
            
            $rus[]= Contents::find($val->id)->delete();
        }
        return $rus;
    }
}
