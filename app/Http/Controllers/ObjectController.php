<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Objects;
use App\Models\Files;
class ObjectController extends Controller
{
    public function getObject()
    {
        return Objects::with('file')->get();
    }
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:255',
        ]);

        $obj =new Objects();
        $obj->name = $request->name;
        $obj->save();
        return $obj;
    }
    public function upload(Request $request)
    {
        $request->validate([
            'file' => 'required',
        ]);
        $url=  $request->file->storeAs('uploads'."/".$request->id ,  $request->file->getClientOriginalName(),'public');
        $save=Files::create(
            [
                'url'=>$url,
                'object_id'=>$request->id
            ]
            );
        return $save;
    }
    public function deleteObject(Request $request)
    {
        $files = Files::where('object_id',$request->id)->get();
        foreach($files as $file){
            Storage::delete($file->url);
            Files::find($file->id)->delete();
            // echo $file;
            // return $file->url;
        }
        return Objects::where('id', $request->id)->delete();
    }
    public function multidelete(Request $request){
        $data = json_decode($request->data);
        $rus=[];
        foreach ($data as $val) {
            // return $val->id;
            $files = Files::where('object_id',$val->id)->get();
            foreach($files as $file){
                Storage::delete($file->url);
                Files::find($file->id)->delete();
                // echo $file;
                // return $file->url;
            }
            $rus[]= Objects::where('id', $val->id)->delete();
        }
        return $rus;
    }
    public function update_object(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required|min:3|max:255',
        ]);

        $data = [
            'name' => $request->name,
        ];
        //bazaga shunaqa nom bilan save qilingan yani stores_id bilan
        $Object = Objects::find($request->id)->update($data);
        return $Object;
    }
    public function filedelete(Request $request)
    {
        $file = Files::where('id',$request->id)->first();
        Storage::delete($file->url);
        // Files::find($file->id)->delete();
        return Files::find($file->id)->delete();
    }
};
