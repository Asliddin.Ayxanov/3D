<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contents extends Model
{
    protected $fillable = ['id', 'name', 'text', 'object_id', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Categories','category_id');
    }
    public function object()
    {
        return $this->belongsTo('App\Models\Objects','object_id');
    }
    protected $hidden = [
        'object_id', 
        'category_id'
    ];
}
