<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objects extends Model
{
    protected $fillable = ['id', 'name'];
    public function file()
    {
        return $this->hasMany('App\Models\Files','object_id');
    }
}
