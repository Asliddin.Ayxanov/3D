<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['id', 'name'];

    public function content()
    {
        return $this->hasMany('App\Models\Contents','category_id');
    }
}
