<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ObjectController;
use App\Http\Controllers\ContentController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('/category')
    ->group(function () {
        Route::get('/get_category', [CategoryController::class,'GetCategory']);
        Route::get('/store', [CategoryController::class,'store']);
        Route::post('/create_category', [CategoryController::class,'CreateCategory']);
        Route::post('/delete_category', [CategoryController::class,'DeleteCategory']);
        Route::post('/update_category', [CategoryController::class,'UpdateCategory']);
        Route::post('/multidelete', [CategoryController::class,'multidelete']);
    });
Route::prefix('/objects')
    ->group(function () {
        Route::get('/', [ObjectController::class,'getObject']);
        Route::post('/upload', [ObjectController::class,'upload']);
        Route::post('/update_object', [ObjectController::class,'update_object']);
        Route::post('/create', [ObjectController::class,'create']);
        Route::post('/deleteObject', [ObjectController::class,'deleteObject']);
        Route::post('/multidelete', [ObjectController::class,'multidelete']);
        Route::post('/filedelete', [ObjectController::class,'filedelete']);

    });
Route::prefix('/content')
    ->group(function () {
        Route::get('/', [ContentController::class,'getContent']);
        Route::post('/upload', [ContentController::class,'upload']);
        Route::post('/create_content', [ContentController::class,'create']);
        Route::post('/update_content', [ContentController::class,'update']);
        Route::post('/delete_content', [ContentController::class,'delete_content']);
        Route::post('/multidelete', [ContentController::class,'multidelete']);
    });
